# Custom Emoji

This project is used as stop-gap solution to store custom emoji on GitLab.com itself.

Images are stored using Git LFS, which means they end up on object storage.

## Instructions

To add custom emoji:

1. Add the image to the `img/` directory in this repository.
1. Locate the URL to the raw image URL from the [tree](https://gitlab.com/custom_emoji/custom_emoji/-/tree/main/img).
1. Visit the [GraphQL explorer](https://gitlab.com/-/graphql-explorer)
1. Submit the following mutation:

    ```graphql
    mutation {
      createCustomEmoji(input: {
        groupPath: "gitlab-org",
        name: "partyparrot",
        url: "https://gitlab.com/custom_emoji/custom_emoji/-/raw/main/img/partyparrot.gif",
      }) {
        customEmoji {
          name
        }
        errors
      }
    }
    ```
1. Repeat for `groupPath: "gitlab-com"`
1. Done!
