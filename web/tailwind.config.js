/** @type {import("tailwindcss").Config} */
export default {
  content: ["./index.liquid"],
  theme: {
    extend: {
      colors: {
        "brand-dark": "#e24329",
        "brand-medium": "#fc6d26",
        "brand-light": "#fca326",
      },
    },
  },
  plugins: [],
};
