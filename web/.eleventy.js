import path from "path";
import fs from "fs";

const REPO_EMOJIS_DIR = "../img";
const SITE_EMOJIS_DIR = "emojis";
// Emoji names that may appear in the header randomly
const RANDOM_EMOJI_NAMES = [
  "awesome",
  "chefkiss",
  "danceduck",
  "dancing_duck",
  "dancing_pikachu",
  "dancing_lisa",
  "yay",
];
const OUTPUT_DIR = "public";

export default function (eleventyConfig) {
  // Wait for styles to rebuild
  eleventyConfig.setWatchThrottleWaitTime(500);
  eleventyConfig.setServerOptions({
    port: 3000,
    showAllHosts: true,
    // Always page reload in case of stylesheet changes
    domDiff: false,
  });

  eleventyConfig.addPassthroughCopy({ [REPO_EMOJIS_DIR]: SITE_EMOJIS_DIR });
  eleventyConfig.addPassthroughCopy({ static: "." });

  eleventyConfig.addShortcode("stylesheet", stylesheetShortcode);

  eleventyConfig.addGlobalData("emojis_dir", SITE_EMOJIS_DIR);
  eleventyConfig.addGlobalData("emojis_db", buildEmojisDb());
  eleventyConfig.addGlobalData("random_emoji_names", RANDOM_EMOJI_NAMES);
  eleventyConfig.addGlobalData(
    "site_base_dir",
    process.env.SITE_BASE_DIR ?? "/"
  );

  return {
    dir: {
      input: "index.liquid",
      output: OUTPUT_DIR,
    },
  };
}

/**
 * @typedef EmojiDb
 * @type {Object.<string, Emoji>}
 */

/**
 * @typedef Emoji
 * @type {object}
 * @property {string} name - e.g. emoji
 * @property {string} filename - e.g. emoji.gif
 * @property {string} path - e.g. public/emoji.gif
 */

/**
 * @returns {EmojiDb}
 */
function buildEmojisDb() {
  if (!fs.existsSync(REPO_EMOJIS_DIR)) {
    throw new Error("Could not find emojis directory");
  }

  /** @type {EmojiDb} */
  const emojis = {};
  const filenames = fs.readdirSync(REPO_EMOJIS_DIR, {
    withFileTypes: false,
    recursive: false,
  });

  for (const filename of filenames) {
    const baseFilename = path.parse(filename).name;

    if (baseFilename === "") {
      throw new Error("Could not get base emoji name: " + filename);
    }

    emojis[baseFilename] = {
      name: baseFilename,
      filename,
      path: path.join(SITE_EMOJIS_DIR, filename),
    };
  }

  return emojis;
}

/**
 * Inline a stylesheet file in prod
 *
 * @param {string} inputFile
 * @returns {string}
 */
function stylesheetShortcode(inputFile) {
  if (
    process.env.ELEVENTY_RUN_MODE !== "build" ||
    process.env.ELEVENTY_ENV !== "production"
  ) {
    return `<link href="${inputFile}" rel="stylesheet">`;
  }

  const file = path.join(OUTPUT_DIR, inputFile);

  if (!fs.existsSync(file)) {
    throw new Error(`Could not find file: ${inputFile} at ${file}`);
  }

  const content = fs.readFileSync(file);
  fs.unlinkSync(file);

  return `<style>${content}</style>`;
}
